package ictgradschool.industry.lab14.ex05;

import java.awt.*;

/**
 * Class to represent a simple rectangle.
 * 
 * @author Ian Warren
 */
public class LoadingShape extends Shape {
	/**
	 * Default constructor that creates a ictgradschool.industry.lab12.bounce.RectangleShape instance whose instance
	 * variables are set to default values.
	 */
	public LoadingShape() {
		super();
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed and direction for horizontal axis.
	 * @param deltaY speed and direction for vertical axis.
	 */
	public LoadingShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed (pixels per move call) and direction for horizontal
	 *        axis.
	 * @param deltaY speed (pixels per move call) and direction for vertical
	 *        axis.
	 * @param width width in pixels.
	 * @param height height in pixels.
	 */
	public LoadingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
		super(x,y,deltaX,deltaY,width,height);
	}
	
	/**
	 * Paints this ictgradschool.industry.lab12.bounce.RectangleShape object using the supplied ictgradschool.industry.lab12.bounce.Painter object.
	 */
	@Override
	public void paint(Painter painter) {
		painter.setColor(Color.GRAY);
		painter.fillRect(fX,fY,fWidth,fHeight);
		painter.drawCenteredText("Loading",fX,fY,fWidth,fHeight);
	}
}
